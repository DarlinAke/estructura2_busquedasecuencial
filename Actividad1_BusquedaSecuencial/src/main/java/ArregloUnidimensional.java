import java.util.Scanner;

/**
 *
 * @author Dell
 */
public class ArregloUnidimensional {
    
    // Darlin Edey Cahuich Ake 
    
    public static void main(String[] args) {
        
          Scanner sc = new Scanner(System.in); //El programa cuenta con un scanner el cual permite al usuario ingresar datos
          int Buscar,posicion;
          int[] arreglo = {22, 60, 100, 12, 30, 10, 15, 35, 70, 13, 47}; //El arreglo unidimensional se encuentra ingresado
          System.out.println("Escribe el  numero que quieres buscar: ");
          Buscar = sc.nextInt(); // El usuario tiene que ingresar el dato
          posicion = arregloUnidimensional(arreglo, Buscar); //El programa recorre dentro de nuestro arreglo para poder encontrar el dato solicitado
          System.out.println("El numero se encuentra en la posición:" +posicion);
    }
    
    public static int arregloUnidimensional(int[] array, int x) {
        int posicion = -1; //En este apartado si el numero ingresado no se encuentra el programa nos mandara el -1, indicandome que no existe 
        for (int i = 0; i < array.length; i++) { 
        
            if (array[i] == x) {
                posicion = i;
                break;  
            }
        }
        return posicion;
    }
}


